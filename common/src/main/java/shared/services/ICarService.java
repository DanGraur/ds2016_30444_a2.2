package shared.services;

import shared.entities.Car;
import shared.exceptions.InvalidDataException;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by noi on 11/11/2016.
 */
public interface ICarService extends Remote{

    double computePrice(Car c) throws RemoteException, InvalidDataException;

    double computeTax(Car c) throws RemoteException, InvalidDataException;
}
