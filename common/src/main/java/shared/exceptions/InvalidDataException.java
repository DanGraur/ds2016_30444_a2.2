package shared.exceptions;

import java.io.Serializable;

/**
 * Created by noi on 11/11/2016.
 */
public class InvalidDataException extends Exception implements Serializable{

    public InvalidDataException() {
        super("The data provided is invalid");
    }

    public InvalidDataException(String message) {
        super(message);
    }
}
