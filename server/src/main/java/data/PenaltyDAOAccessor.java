package data;

import entity.Penalty;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PenaltyDAOAccessor {

    private static PenaltyDAO penaltyDAO;

    private PenaltyDAOAccessor() {
    }

    public static PenaltyDAO getInstance() {
        if (penaltyDAO == null)
            penaltyDAO = new PenaltyDAOImpl(new Configuration().configure().buildSessionFactory());

        return penaltyDAO;
    }

    private static class PenaltyDAOImpl implements PenaltyDAO {

        SessionFactory factory;

        PenaltyDAOImpl(SessionFactory factory) {
            this.factory = factory;
        }

        @Override
        public List<Penalty> getPenalties() {
            Session session = factory.openSession();
            Transaction tx = null;
            List<Penalty> penalties = null;

            try {
                tx = session.beginTransaction();

                penalties = session.createQuery("FROM Penalty").list();

                tx.commit();
            } catch (HibernateException e) {
                if (tx != null)
                    tx.rollback();
            } finally {
                session.close();
            }

            return penalties;
        }

    }
}
