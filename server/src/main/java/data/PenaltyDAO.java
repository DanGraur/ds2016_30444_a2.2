package data;

import entity.Penalty;

import java.util.List;

/**
 * Created by noi on 11/11/2016.
 */
public interface PenaltyDAO {

    List<Penalty> getPenalties();
}
