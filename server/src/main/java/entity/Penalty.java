package entity;

/**
 * Created by noi on 11/11/2016.
 */
public class Penalty {
    private int id;
    private int min;
    private int max;
    private int penalty;

    @Override
    public String toString() {
        return "Penalty{" +
                "id=" + id +
                ", min=" + min +
                ", max=" + max +
                ", penalty=" + penalty +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }
}
