package logic;

import data.PenaltyDAOAccessor;
import entity.Penalty;

import java.util.List;

/**
 * Created by noi on 11/11/2016.
 */
public class CarPenaltyUtility {

    public static int findPenalty(int engineSize) {
        List<Penalty> penalties = PenaltyDAOAccessor.getInstance().getPenalties();

        for (Penalty p : penalties)
            if (engineSize >= p.getMin() && engineSize <= p.getMax())
                return p.getPenalty();

        return 8;
    }
}
