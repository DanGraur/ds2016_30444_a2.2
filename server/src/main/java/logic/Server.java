package logic;

import shared.services.ICarService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by noi on 11/11/2016.
 */
public class Server {
    private static final int PORT = 4459;

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setProperty("java.security.policy","file:./server.policy");

            System.setSecurityManager(new SecurityManager());
        }

        try {
            // Create the remote object, which will be used to implement both methods
            ICarService carService = new CarService();

            // Set-up these objects as remote, and in return receive the stubs placed in the naming service
            // These will be set-up on an anonymous port
            ICarService stubCarService = (ICarService) UnicastRemoteObject.exportObject(carService, 0);

            // Create the registry at the given port
            Registry registry = LocateRegistry.createRegistry(PORT);

            // Bind the stubs on the given port
            registry.rebind("carService", stubCarService);

        } catch (RemoteException e) {
            System.out.println("THE SERVER HAS ENCOUNTERED A REMOTE EXCEPTION ERROR");

            e.printStackTrace();
        }
    }
}
