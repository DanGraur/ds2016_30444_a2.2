package logic;

import shared.entities.Car;
import shared.exceptions.InvalidDataException;
import shared.services.ICarService;

import java.rmi.RemoteException;

/**
 * Created by noi on 11/11/2016.
 */
public class CarService implements ICarService {

    @Override
    public double computePrice(Car c) throws RemoteException, InvalidDataException {
        if (c.getPrice() < 0.0)
            throw new InvalidDataException("The price must be greater than 0.0");
        if (c.getYear() < 1800)
            throw new InvalidDataException("The car must have been bought after 1800");

        return c.getPrice() - (c.getPrice() / 7.0) * (2016 - c.getYear());
    }

    @Override
    public double computeTax(Car c) throws RemoteException, InvalidDataException {
        if(c.getEngineSize() < 0.0)
            throw new InvalidDataException("The engine size of the car cannot be negative");

        return (c.getEngineSize() / 200) * CarPenaltyUtility.findPenalty(c.getEngineSize());
    }
}
