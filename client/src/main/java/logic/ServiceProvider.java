package logic;

import shared.entities.Car;
import shared.exceptions.InvalidDataException;
import shared.services.ICarService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by noi on 11/12/2016.
 */
public class ServiceProvider {
    private static final String HOST = "localhost";
    private static final int PORT = 4459;

    private static ServiceProvider serviceProvider = new ServiceProvider();

    private ServiceProvider() {
        System.setProperty("java.security.policy","file:./client.policy");

        System.setSecurityManager(new SecurityManager());
    }

    public static ServiceProvider getInstance() {
        return serviceProvider;
    }

    public double computePrice(Car c) throws RemoteException, NotBoundException, InvalidDataException {
        Registry registry = LocateRegistry.getRegistry(HOST, PORT);

        ICarService priceService = (ICarService) registry.lookup("carService");

        return priceService.computePrice(c);

    }

    public double computeTax(Car c) throws RemoteException, NotBoundException, InvalidDataException {
        Registry registry = LocateRegistry.getRegistry(HOST, PORT);

        ICarService priceService = (ICarService) registry.lookup("carService");

        return priceService.computeTax(c);
    }
}
