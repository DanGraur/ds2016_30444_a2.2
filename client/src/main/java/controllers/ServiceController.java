package controllers;

import graphics.ServiceInterface;
import logic.ServiceProvider;
import shared.entities.Car;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by noi on 11/12/2016.
 */
public class ServiceController {
    private ServiceInterface serviceInterface;

    public ServiceController() {
        serviceInterface = new ServiceInterface();

        serviceInterface.addButtonPriceActionListener(new PriceTriggerHandler());
        serviceInterface.addButtonTaxActionListener(new TaxTriggerHandler());
    }

    public void displayResult(String result) {
        if (result != null) {
            serviceInterface.setResultArea(result);
        }
    }

    public void displayErrorMessage(String message) {
        serviceInterface.clear();
        JOptionPane.showMessageDialog(serviceInterface, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    private class TaxTriggerHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Car car = new Car(Integer.parseInt(serviceInterface.getCarYear()),
                        Integer.parseInt(serviceInterface.getCarEngineSize()),
                        Double.parseDouble(serviceInterface.getCarPrice()));

                String carTaxResult = "The tax for the car is: " + ServiceProvider.getInstance().computeTax(car);

                displayResult(carTaxResult);

            } catch (Exception exception) {
                displayErrorMessage(exception.getMessage());
            }
        }
    }

    private class PriceTriggerHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Car car = new Car(Integer.parseInt(serviceInterface.getCarYear()),
                        Integer.parseInt(serviceInterface.getCarEngineSize()),
                        Double.parseDouble(serviceInterface.getCarPrice()));

                String carPriceResult = "The price for the car is: " + ServiceProvider.getInstance().computePrice(car);

                displayResult(carPriceResult);

            } catch (NumberFormatException exception) {
                displayErrorMessage("Invalid input data provided " + exception.getMessage());
            }  catch (Exception exception) {
                displayErrorMessage(exception.getMessage());
            }

        }

    }
}
