package graphics;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class ServiceInterface extends JFrame {
    private static final long serialVersionUID = 1L;

    private JPanel contentPane;

    private JTextField textYear;
    private JTextField textEngineSize;
    private JTextField textInitialPrice;

    private JButton buttonPrice;
    private JButton buttonTax;

    private JTextArea resultArea;

    public ServiceInterface() {
        setTitle("Car Service - RMI");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 265, 310);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblInsertNewStudent = new JLabel("Car Information:");
        lblInsertNewStudent.setBounds(10, 11, 170, 14);
        contentPane.add(lblInsertNewStudent);

        JLabel labelYear = new JLabel("Year");
        labelYear.setBounds(10, 36, 60, 14);
        contentPane.add(labelYear);

        JLabel labelEngineSize = new JLabel("Engine Size");
        labelEngineSize.setBounds(10, 61, 170, 14);
        contentPane.add(labelEngineSize);

        JLabel labelInitialPrice = new JLabel("Initial Price");
        labelInitialPrice.setBounds(10, 86, 170, 14);
        contentPane.add(labelInitialPrice);

        textYear = new JTextField();
        textYear.setBounds(90, 33, 150, 20);
        contentPane.add(textYear);
        textYear.setColumns(10);

        textEngineSize = new JTextField();
        textEngineSize.setBounds(90, 58, 150, 20);
        contentPane.add(textEngineSize);
        textEngineSize.setColumns(10);

        textInitialPrice = new JTextField();
        textInitialPrice.setBounds(90, 83, 150, 20);
        contentPane.add(textInitialPrice);
        textInitialPrice.setColumns(10);

        buttonPrice = new JButton("Get Price");
        buttonPrice.setBounds(10, 113, 110, 23);
        contentPane.add(buttonPrice);

        buttonTax = new JButton("Get Tax");
        buttonTax.setBounds(130, 113, 110, 23);
        contentPane.add(buttonTax);

        resultArea = new JTextArea();
        resultArea.setBounds(10, 141, 230, 120);
        resultArea.setBorder(BorderFactory.createLineBorder(Color.gray));
        resultArea.setEditable(false);
        contentPane.add(resultArea);

        this.setVisible(true);
    }

    public void addButtonPriceActionListener(ActionListener e) {
        buttonPrice.addActionListener(e);
    }

    public void addButtonTaxActionListener(ActionListener e) {
        buttonTax.addActionListener(e);
    }

    public void setResultArea(String result) {
        resultArea.setText(result);
    }

    public String getCarYear() {
        return textYear.getText();
    }
    public String getCarEngineSize() {
        return textEngineSize.getText();
    }

    public String getCarPrice() {
        return textInitialPrice.getText();
    }

    public void clear() {
        textYear.setText("");
        textInitialPrice.setText("");
        textEngineSize.setText("");
        resultArea.setText("");
    }
}
